#  安装



## 获得license文件

```bash
执行下面命令获取机器码 

sudo chmod +x GetID/GetID && ./GetID/GetID --appid gb28181

根据机器码从公司拿到license文件

将license覆盖lic目录下lic.lic文件
```



## 安装docker和docker-compose，docker配置开机启动

自行安装，过程略



## 安装程序

```bash
解压程序：

sudo tar -Jxvf bin.tar.xz

进入bin目录,执行如下命令安装：

sudo ./install.sh
```



## 配置文件修改

docker-compose.yml文件修改

```js
"-PublicIP","172.51.3.10",
改为本机IP
```

config/config.json文件修改

```json
"realm": "410118",
"localDeviceID": "41011800002001000002",
"localAddr": "10.10.1.7",
"publicAddress": "10.10.1.7",
"localPort": 5060,

realm：为localDeviceID的前6位，也就是邮编
localDeviceID：为本机ID，需遵循GB28181的ID定义规则
localAddr：为本机IP
publicAddress：为外网IP，若无外网，则配置为本机IP
localPort：本机监听端口，下级向此端口注册
```



## 启动

下面命令可启动程序，且开机自启

```bash
sudo chmod +x start.sh && sudo ./start.sh
```



下面命令验证是否启动成功，执行下面命令，如果STATUS都是Up状态则正常启动

```bash
sudo docker ps

CONTAINER ID        IMAGE                                       COMMAND                  CREATED             STATUS              PORTS               NAMES
e4062b1ddb79        gostream/docker_gb28181_signalling:latest   "node app.js"            4 seconds ago       Up 3 seconds                            gb28181-signalling-app
9531b3778d59        gostream/docker_gb28181_signalling:latest   "node app_query.js"      4 seconds ago       Up 3 seconds                            gb28181-signalling-app-query
f54fe0c9e8bd        redis:alpine3.12                            "docker-entrypoint.s…"   4 seconds ago       Up 4 seconds                            redis
cfde8629f79f        gostream/docker-gb28181-media:latest        "/gb28181-media -Pub…"   4 seconds ago       Up 4 seconds                            gb28181-stream
```

## 其他

停止

```bash
sudo chmod +x stop.sh && sudo ./stop.sh
```

配置防火墙

```js
api端口：8888/tcp 9999/tcp

流媒体端口：554/tcp 1935/tcp 1936/tcp

信令端口：5060/tcp 5060/udp   此端口同config/config.json中的localPort

rtp端口：40000-50000/udp   此端口同docker-compose.yml中的UDPPortMin~UDPPortMax
```

